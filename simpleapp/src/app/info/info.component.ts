import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  @Input() name:string = '';
  quantity = 0;
  products : string[] = [];
  selectedProduct = '';

  constructor() { }

  ngOnInit(): void {
    this.quantity = 1;
    this.products  = ['Empire Strikes Back', 'The Last Jedi', 'A New Hope'];
    this.selectedProduct = this.products[0];
  }

  onSubmit(){
    console.log("Your order has been placed. Product: " + this.selectedProduct + ", Quantity: " + this.quantity);
  }

  newInfo(){

  }
}
